// grid dummy data
var dummyData = {
	//Herramientas	
	"Todos": "<div class=\"contenido oculto\"><section id=\"accesos-directos\"><h3>Accede a:</h3><article><a href=\"https://campusvirtual.up.edu.pe/academico\"><img src=\"img/up-sap.jpg\" alt=\"\"><h4>Servicios Académicos Pregrado</h4></a></article><article><a href=\"https://autoservicio1.up.edu.pe/ss/Home.aspx\"><img src=\"img/up-power-campus.jpg\" alt=\"\"><h4>Power Campus</h4></a></article><article><a href=\"https://srvnetappseg.up.edu.pe/UPPagoVirtual/frmLogin.aspx\"><img src=\"img/up-pagos-virtuales.jpg\" alt=\"\"><h4>Pagos Virtuales</h4></a></article></section><section id=\"acceso-blackboard\"><h3>&nbsp;</h3><article class=\"bg-white\"><div class=\"block bg-gray\"><img src=\"img/logo-bb.png\" alt=\"\"><a target=\"_blank\" href=\"http://bb.up.edu.pe/\" class=\"btn-campus-blackboard\">Ingresar</a></div><div class=\"bg-blue\"><div class=\"bloque-enlace\"><a target=\"_blank\" href=\"https://srvnetappseg.up.edu.pe/siserpwd/\">¿Olvidaste tu contraseña UP?</a><span>Si no recuerdas tu contraseña y necesitas generar una nueva.</span></div><div class=\"bloque-enlace\"><a target=\"_blank\" href=\"https://srvnetappseg.up.edu.pe/siserpwd/cambioContrasenia.aspx\">Cambiar contraseña</a><span>Si tienes tu contraseña pero quieres reemplazarla por una nueva.</span></div><div class=\"container-img\"><img src=\"img/login_icon.png\" alt=\"\"></div></div></article></section></div>",
	
	"Blackboard":"<li class=\"enlace\"><h2>Blackboard</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"http://bb.up.edu.pe/\" target=\"_blank\">Visitar Enlace</a><div class=\"bloque-enlace\"><a href=\"https://srvnetappseg.up.edu.pe/siserpwd/\">¿Olvidaste tu contraseña UP?</a><span>Si no recuerdas tu contraseña y necesitas generar una nueva.</span></div><div class=\"bloque-enlace\"><a href=\"https://srvnetappseg.up.edu.pe/siserpwd/cambioContrasenia.aspx\">Cambiar contraseña</a><span>Si tienes tu contraseña pero quieres reemplazarla por una nueva.</span></div></li>",

	"Power Campus":"<li class=\"enlace\"><h2>Power Campus</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"https://autoservicio.up.edu.pe/\" target=\"_blank\">Visitar Enlace</a> </li>",

	"¿Cómo usar Blackboard?":"<li class=\"enlace\"><h2>¿Cómo usar Blackboard?</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"#\" target=\"_blank\">Visitar Enlace</a> </li>",

	"¿Cómo usar Power Campus?":"<li class=\"enlace\"><h2>¿Cómo usar Power Campus?</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"#\" target=\"_blank\">Visitar Enlace</a> </li>",

	"¿Cómo cargar notas?":"<li class=\"enlace\"><h2>¿Cómo cargar notas?</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"#\" target=\"_blank\">Visitar Enlace</a> </li>",


	//Academicas
	"Horarios":"<li class=\"enlace\"><h2>Horarios</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"https://autoservicio1.up.edu.pe/ss/Records/ClassSchedule.aspx\" target=\"_blank\">Visitar Enlace</a> </li>",

	"Toma de asistencia":"<li class=\"enlace\"><h2>Toma de asistencia</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"#\" target=\"_blank\">Visitar Enlace</a> </li>",

	"Ingresar notas":"<li class=\"enlace\"><h2>Ingresar notas</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"#\" target=\"_blank\">Visitar Enlace</a> </li>",


	//Servicios
	"Estacionamiento":"<li class=\"enlace\"><h2>Estacionamiento</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"http://estaciona.up.edu.pe/conteoplazas/\" target=\"_blank\">Visitar Enlace</a> </li>",

	"Fechas importantes":"<li class=\"enlace\"><h2>Fechas importantes</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"#\" target=\"_blank\">Visitar Enlace</a> </li>",

	"Números importantes":"<li class=\"enlace\"><h2>Números importantes</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"#\" target=\"_blank\">Visitar Enlace</a> </li>",

	"Calendario 2016-1":"<li class=\"enlace\"><h2>Calendario 2016-1</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"#\" target=\"_blank\">Visitar Enlace</a> </li>",


	//Contacto de servicio al docente
	"Boletas":"<li class=\"enlace\"><h2>Boletas</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"#\" target=\"_blank\">Visitar Enlace</a> </li>",

	"Reglamentos":"<li class=\"enlace\"><h2>Reglamentos</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"#\" target=\"_blank\">Visitar Enlace</a> </li>",

	"Documentos de discusión":"<li class=\"enlace\"><h2>Documentos de discusión</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, tempore, beatae, dolor atque eveniet iure dicta laborum aliquid enim quasi dolorum suscipit reiciendis cum quos harum maxime ipsa soluta ex.</p><a class=\"btn-campus-up\" href=\"#\" target=\"_blank\">Visitar Enlace</a> </li>",
}